
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//Jhustin Ismael Arias Perez 4-A T/M

namespace TemaDelegados
{

	//Estos son los delegados y sus caracteristicas

	delegate void Reservas(int Rkilos);
	delegate void Descongelado(int Dgrados);

	Class Refri
	{

		private int kiloAlimento = 0;
		private int grados = 0;		

		//Variables de los delegados
		private Reservas delReservas;
		private Descongelado delDescongelado;

		public Refri(int Rkilos, int Dgrados){

		kiloAlimento = Rkilo;
		grados = Dgrados;
		
		}

		//Referencias de las variables

		public void AdicionarReservas(delReservas pMetodo){

		delReservas += pMetodo;
		}

		public void EliminarReservas(delReservas pMetodo){

		delReservas -= pMetodo;
		}

		public void AdicionarDescongelado(delDescongelado pMetodo){

		delDescongelado += pMetodo;
		}

		//Propiedades

		public int Kilos { get { return kiloAlimento; } set { kiloAlimento = value; }}
		public int Grados { get { return grados; } set { grados = value; }}

		public void Trabajar (int pConsumo)
		{
		
		kiloAlimento -= pConsumo;
		grados += 1;

		Console.WriteLine("{0} kilos, {1} grados", kiloAlimento, grados);

		//Eventos y condiciones

		if(kiloAlimento < 10){

		delReserva(kiloAlimento);
		
		} 

		if(grados > 0){

		delDescongelado(grados);
		
		} 

	}
	}
}