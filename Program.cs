
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//Jhustin Ismael Arias Perez 4-A T/M

namespace TemaDelegados
{

	//Este es un delegado y sus caracteristicas

	public delegate void Delegado(string m);

	Class Program
	{

		static void Main(string[] args)
		{
			
		//Se crea el objeto delegado y se refencia
		Delegado delegado_nuevo = new Delegado(Radio.MetodoRadio);

		//Se usa el metodo referenciado
		delegado_nuevo("Uso del metodo en Radio");

		//Se invoca el otro metodo
		delegado_nuevo = new Delegado(Pastel.MostrarPastel);		

		//Se usa el metodo referenciado
		delegado_nuevo("Uso del metodo en Pastel");

	}
}